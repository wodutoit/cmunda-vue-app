"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const camunda_external_task_client_js_1 = require("camunda-external-task-client-js");
const constants_1 = require("./constants");
const config = { baseUrl: constants_1.settings.baseUrl, use: camunda_external_task_client_js_1.logger };
// create a Client instance with custom configuration
const client = new camunda_external_task_client_js_1.Client(config);
// susbscribe to the topic: 'creditScoreChecker'
client.subscribe('FirstTopicSubscription', ({ task, taskService }) => __awaiter(this, void 0, void 0, function* () {
    // Put your business logic
    const processVariables = new camunda_external_task_client_js_1.Variables();
    processVariables.set('resultOfAction', 'Deflector Online');
    // complete the task
    yield taskService.complete(task, processVariables);
}));
//# sourceMappingURL=worker.js.map