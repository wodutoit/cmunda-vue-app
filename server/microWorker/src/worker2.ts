import { Client, logger, Variables } from 'camunda-external-task-client-js';
import { settings } from './constants';

const config = { baseUrl: settings.baseUrl, use: logger };

// create a Client instance with custom configuration
const client = new Client(config);

client.subscribe('SecondTopicSubscription', async ({ task, taskService }) => {
  // Put your business logic
  const processVariables = new Variables();
  processVariables.set('resultOfAction', 'Deflector Online');
  // complete the task
  await taskService.complete(task, processVariables);
});
